<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Task;
use App\Models\Account;
use App\Models\Project;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\Job as JobResource;
use App\Http\Resources\Task as TaskResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Account as AccountResource;
use App\Http\Resources\Project as ProjectResource;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Insert records in all tables using Eloquent
     *
     * @return void
     */
    public function insertUsingEloquent(): void
    {
        // Insert accounts
        $account1 = Account::create(["name" => "Account 1"]);
        $account2 = Account::create(["name" => "Account 2"]);
        $account3 = Account::create(["name" => "Account 3"]);

        // Insert projects
        $project1 = $account1->projects()->create(["name" => "Project 1", "price" => 50]);
        $project2 = $account2->projects()->create(["name" => "Project 2", "price" => 90]);
        $project3 = $account3->projects()->create(["name" => "Project 3", "price" => 150]);

        // Insert jobs
        $job1 = $project1->jobs()->create(["name" => "Job 1", "amount" => 100]);
        $job2 = $project2->jobs()->create(["name" => "Job 2", "amount" => 120]);
        $job3 = $project3->jobs()->create(["name" => "Job 3", "amount" => 80]);

        // Insert tasks
        $task1 = $job1->tasks()->create(["name" => "Task 1"]);
        $task2 = $job2->tasks()->create(["name" => "Task 2"]);
        $task3 = $job3->tasks()->create(["name" => "Task 3"]);

    }

    /**
     * Git all accounts along with projects, jobs and tasks.
     *
     * @return JsonResource
     */
    public function selectAccounts(): JsonResource
    {
        $accounts = Account::with('projects.jobs.tasks')->get();

        return AccountResource::collection($accounts);

    }

    /**
     * Git all projects along with account, jobs and tasks.
     *
     * @return JsonResource
     */
    public function selectProjects(): JsonResource
    {
        $projects = Project::with('account', 'jobs.tasks')->get();

        return ProjectResource::collection($projects);
    }

    /**
     * Get all jobs along wiht project and tasks.
     *
     * @return JsonResource
     */
    public function selectJobs(): JsonResource
    {
        $jobs = Job::with('project', 'tasks')->get();

        return JobResource::collection($jobs);
    }

    /**
     * Get all tasks along with job.
     *
     * @return JsonResource
     */
    public function selectTasks(): JsonResource
    {
        $tasks = Task::with('job')->get();

        return TaskResource::collection($tasks);
    }

    /**
     * Git tasks that belongs to project with price < 100.
     *
     * @return JsonResource
     */
    public function tasksByProjectPrice(): JsonResource
    {
        $tasks = Task::whereHas(
            'job.project',
            function (Builder $query): void {
                $query->where('price', '<', 100);
            }
        )->get();

        return TaskResource::collection($tasks);
    }
}
