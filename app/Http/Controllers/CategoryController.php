<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Resources\Category as CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryController extends Controller
{
    /**
     * Get all root categories with descendants.
     *
     * @return JsonResource
     */
    public function index(): JsonResource
    {
        $categories = Category::root()->with('descendants')->paginate();

        return CategoryResource::collection($categories);
    }
}
