<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $depth = 4;
        $categories = $this->createCategories(rand(1, 3));

        for ($i = 1; $i < $depth; $i++) {
            $temp = collect([]);

            foreach ($categories as $category) {
                $children = rand(1, 3);
                $temp = $temp->merge($this->createCategories($children, $category));
            }

            $categories = $temp;
        }
    }

    /**
     * Create a given number of child categories for a given parent category.
     *
     * @param int           $children
     * @param null|Category $parent
     *
     * @return Collection
     */
    private function createCategories(int $children = 1, ?Category $parent = null): Collection
    {
        if ($parent === null) {
            $parent = new Category();
        }

        return $parent->children()->saveMany(Category::factory()->count($children)->make());
    }
}
