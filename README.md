# Penta Task
This project is a solution for an interview task by PentaValue

---
### Contents

- [Hierarchical Task](#hierarchical-task)
- [Queries Task](#queries-task)

---
### Hierarchical Task
Create hierarchical database table “just one table” that contains “Categories” and “Sub Categories” with unlimited tree levels, then write a query to select these Categories with all sub
levels.

#### Solution
- A `Category` model is created that has a `one-to-many` relationship with itself, where a `Category` has `parent`, `children` *(The next generation only)*, and `descendants` *(All the generations)*
- A `CategoryFactory` and a `CategorySeeder` are created to fill the database with categories.
- An API endpiont `/api/categories` that calls `CategoryController::index` is created to list all the root categories with their descendants.

### Queries Task
- Create table “accounts” (id – name). “accounts” has many “projects” (id – name – price - account_id). “projects” has many “jobs” (id – name – amount – project_id). “jobs” has many “tasks” (id - name - job_id).
- Create models in Laravel for all tables
- Using Laravel query, Insert records in all tables
- Using Laravel query, select records from all tables
- Using Laravel query, select tasks that belongs to project with price < 100.

#### Solution
- All models are created with all relationships.
- A method named `insertUsingEloquent` in controller `Controller` to insert data in all tables using the models (`Account`, `Project`, `Job`, `Task`).
- Selecting records from all tables is done using the methods:
  - `selectAccounts`
  - `selectProjects`
  - `selectJobs`
  - `selectTasks`
In the controller `Controller`, where all records are selected along with their relationships.
- Tasks that belong to to projects with price < 100 are selected in the method `tasksByProjectPrice` in the controller `Controller`
- API endpoints are created for all the methods.
