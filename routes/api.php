<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Controller;

// Categories
Route::apiResource('categories', CategoryController::class)->only('index');

// Insert
Route::post('insert', [Controller::class, 'insertUsingEloquent']);

// Select
Route::get('accounts', [Controller::class, 'selectAccounts']);
Route::get('projects', [Controller::class, 'selectProjects']);
Route::get('jobs', [Controller::class, 'selectJobs']);
Route::get('tasks', [Controller::class, 'selectTasks']);

// Select tasks that belongs to project with price < 100

Route::get('tasks-by-project-price', [Controller::class, 'tasksByProjectPrice']);
